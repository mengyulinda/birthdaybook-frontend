import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'

class SearchPerson extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate : "",
            endDate : "",
            inrange : []
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }



    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
        
    }



    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        axios
            .get('http://localhost:8080/birthdaybook/searchdates/'+this.state.startDate+'/'+this.state.endDate)
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    inrange : response.data
                })
			})
			.catch(error => {
				console.log(error)
			})
    }
    
    render() {
        return (
            <div>
                <div>
                    <h1>Please Search people in birthday Range ! </h1>
                    <br></br>
                    <br></br>
                    <br></br>

                </div>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <label> Start Date of the Birthday Date </label>
                        <input type = 'text' 
                            name = "startDate"
                            value = {this.state.startDate} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> End Date of the Birthday Date </label>
                        <input type = 'text' 
                            name = "endDate"
                            value = {this.state.endDate} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <button type = 'submit'>Search Peolple</button>
                    </div>
                    <br></br>
                </form>
                {
                    this.state.inrange.length ? 
                    this.state.inrange.map(post => <div key = {post.id}> 
                        <div>
                            ID : {post.id}
                        </div>
                        <div>
                            Name : {post.fullName} 
                        </div>
                        <div>
                            Date of Birth : {post.birthdayDateTime}
                        </div>
                        <br></br>
                        <br></br>
                    </div> 
                    
                    ):
                    null
                }

                <br></br>
                <br></br>
                <div>
                     <Link to ="/postList"> Back To Main Page</Link>
                </div>
            </div>
        );
    }
}

export default SearchPerson;