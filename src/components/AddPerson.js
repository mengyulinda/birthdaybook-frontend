import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'
class AddPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName : "",
            birthdayDateTime : ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount(){
        
        axios
			.get('http://localhost:8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }
    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    
    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        axios
			.post('http://localhost:8080/birthdaybook', this.state)
			.then(response => {
                alert("successfully add the person to the birthday book!")
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }

    
    render() {
        const {fullName, birthdayDateTime} = this.state
        return (
            <div>
                <div>
                    <h1>Please Add a Person ! </h1>
                    <br></br>
                    <br></br>
                    <br></br>

                </div>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <label> Full Name </label>
                        <input type = 'text' 
                            name = "fullName"
                            value = {fullName} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Date Of Birth </label>
                        <input type = 'text' 
                            name = "birthdayDateTime"
                            value = {birthdayDateTime} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <button type = 'submit'>Add Perosn</button>
                    </div>
                    <br></br>
                    <div>
                        <Link to ="/postList"> Back To Main Page</Link>
                    </div>
                </form>
            </div>
        );
    }
}

export default AddPerson;