import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom';
import {
    BrowserRouter as Router,
    Route,
  } from 'react-router-dom'

class ModifyPerson extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id : "",
            fullName : "",
            birthdayDateTime : "",
            posts :[]
            
        };
        //var bodyFormData = new FormData();
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    

    componentDidMount(){
        
        axios
			.get('http://localhost:8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }
    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
        
    }

    
    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        axios
            .put('http://localhost:8080/birthdaybook/'+this.state.id, {id : this.state.id,
            fullName : this.state.fullName,
            birthdayDateTime :  this.state.birthdayDateTime})
			.then(response => {
                alert("successfully modify the person in the birthday book!")
                window.location.reload();
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }




    
    render() {
        return (
            <div>
            <h1>You can modify any record in birthday book now </h1>
            <form onSubmit = {this.handleSubmit}>
                <div>
                    <label> Enter Id Number to modify </label>
                    <input type = 'text' 
                        name = "id"
                        value = {this.state.id} 
                        onChange = {this.handleChange}>
                    </input>
                </div>
                <br></br>

                <div>
                    <label> Enter New Name </label>
                    <input type = 'text' 
                        name = "fullName"
                        value = {this.state.fullName} 
                        onChange = {this.handleChange}>
                    </input>
                </div>
                <br></br>
                <div>
                    <label> Enter New Birth Date </label>
                    <input type = 'text' 
                        name = "birthdayDateTime"
                        value = {this.state.birthdayDateTime} 
                        onChange = {this.handleChange}>
                    </input>
                </div>

                <div>
                    <button type = 'submit'>Modify Perosn</button>
                </div>
                <br></br>
                <div>
                    
                    {
                    this.state.posts.length ? 
                    this.state.posts.map(post => <div key = {post.id}> 
                        <div>
                            ID : {post.id}
                        </div>
                        <div>
                            Name : {post.fullName} 
                        </div>
                        <div>
                            Date of Birth : {post.birthdayDateTime}
                        </div>
                        <br></br>
                        <br></br>
                    </div> 
                    
                    ):
                    null
                }
                </div>
            </form>
            <br></br>
            <br></br>
            <div>
                <Link to ="/postList"> Back To Main Page</Link>
            </div>
        </div>
        );
    }
}

export default ModifyPerson;