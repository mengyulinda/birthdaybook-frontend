import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom'

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

        //this.handleChange = this.handleChange.bind(this)
        //this.handleSubmit = this.handleSubmit.bind(this)
    }

    render(){
        return(
        <div>
            <div>
                <h1>Welcome to our birthday book!</h1>
            </div>
            <div>
            <Link to = "/postlist" >you can see who are in our birthday book now</Link>
          </div>
        </div>
        );
    }
}

export default HomePage;
