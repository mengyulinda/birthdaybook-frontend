import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'

class PostList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts :[]
        }
    }

    componentDidMount(){
        
        axios
			.get('http://localhost:8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }
    
    render() {
        const {posts} = this.state
        return (
            <div>
                <h1>List of Person in Birthday Book</h1>
                <br></br>
                <br></br>
                {
                    posts.length ? 
                    posts.map(post => <div key = {post.id}> 
                        <div>
                            ID : {post.id}
                        </div>
                        <div>
                            Name : {post.fullName} 
                        </div>
                        <div>
                            Date of Birth : {post.birthdayDateTime}
                        </div>
                        <br></br>
                        <br></br>
                    </div> 
                    
                    ):
                    null
                }
                <div>
                    <Link to ="/addPerson"> Add another person to birthday book!</Link>
                    <br></br>
                    <br></br>
                    <Link to ="/deletePerson"> Delete the person in birthday book!</Link>
                    <br></br>
                    <br></br>
                    <Link to ="/modifyPerson"> Modify a person in birthdaybook!</Link>
                    <br></br>
                    <br></br>
                    <Link to ="/searchPerson"> Search people in birthdaybook!</Link>
                </div>

            </div>
        );
    }
}

export default PostList;