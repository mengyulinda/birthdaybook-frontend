import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'

class DeletePerson extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id : "",
            posts :[]
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
    }

    componentDidMount(){
        
        axios
			.get('http://localhost:8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }

    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    
    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        axios
			.delete('http://localhost:8080/birthdaybook/'+this.state.id)
			.then(response => {
                alert("successfully delete the person in the birthday book!")
                window.location.reload();
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }
    render() {
        
        return (
            <div>
                <h1>You can delete any record in birthday book now </h1>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <label> Enter Id Number </label>
                        <input type = 'text' 
                            name = "id"
                            value = {this.state.id} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <button type = 'submit'>Delete Perosn</button>
                    </div>
                    <br></br>
                    <div>
                        {
                            this.state.posts.length ? 
                            this.state.posts.map(post => <div key = {post.id}> 
                                <div>
                                    ID : {post.id}
                                </div>
                                <div>
                                    Name : {post.fullName} 
                                </div>
                                <div>
                                    Date of Birth : {post.birthdayDateTime}
                                </div>
                                <br></br>
                                <br></br>
                            </div> 
                            
                            ):
                            null
                        }
                    </div>
                </form>
                <br></br>
                <br></br>
                <div>
                     <Link to ="/postList"> Back To Main Page</Link>
                </div>

            </div>
        );
    }
}

export default DeletePerson;